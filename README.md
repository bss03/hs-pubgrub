# hs-pubgrub

See <https://medium.com/@nex3/pubgrub-2fb6470504f> for what PubGub is.  This is
a Haskell implementation.

Any errors should be assumed to be introduced by this implementation initially,
and should not reflect poorly on Natalie Weizenbaum, the PubGrub specification,
or any other PubGrub implemenation or developers of that implementation.
